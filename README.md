# PDF Reporting component

This component provides PDF generation functionality that can be used in Java projects.

### Steps to integrate

##### Step 1:
In your `maven` project add the following dependency in your "`pom.xml`"

	<dependencies>
		<dependency>
			<groupId>org.ishafoundation.itext</groupId>
			<artifactId>reporting-component</artifactId>
			<version>1.0.0</version>
		</dependency>
		....
	</dependencies>

Step 2:
Create a freemarker template with your design that PDF needs to be generated and save it in a directory. (e.g `src/main/resources/templates/template.ftl`)

Step 3: 
In your class, do the following 

    HtmlPdfConverter pdfConverter = new DefaultHtmlPdfConverter();
    
    //Populate the freemarker data model
    Map<String, Object> ftlModel = populateDataModel();
    
    //Provide the freemarkerfile and PDF file with data model
    pdfConverter.createPdf(ftlFilePath, ftlModel, pdfFile );
PDF file would be generated with the file path specified above.

Full Example: invoice-example (examples/invoice-example)

### Features
This library provides following features

 1. HTML to PDF 
 2. Password Protection
 3. Digital Signature (Comprehensive example with all the features)
 4. Allow/Disallow copying of PDF.
 5. Allow/Disallow printing of PDF.
 
 
 ### Basic PDF generation Demo Video
 
 [Demo](PdfProjectGeneration.mp4)



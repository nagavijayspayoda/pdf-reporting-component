package org.ishafoundation.invoice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.ishafoundation.pdf.HtmlPdfConverter;
import org.ishafoundation.pdf.PdfConfiguration;
import org.ishafoundation.pdf.impl.DefaultHtmlPdfConverter;
/**
 * This example provides a simple use of creating an PDF invoice with disable PDF copying but allow printing
 * configuration from freemarker template (invoice.ftl) 
 * @author nagavijay.s
 *
 */
public class InvoiceDisableCopyAllowPrintingExample {
	
	public static void main(String[] args) throws IOException {
		HtmlPdfConverter pdfConverter = new DefaultHtmlPdfConverter();
		String ftlFilePath = "src/main/resources/templates/invoice.ftl";
		Map<String, Object> ftlModel = populateDataModel();
		
		String pdfFile = "generated_withdisablecopy_allowprinting.pdf" ;
		PdfConfiguration config = createPdfConfigurationWithDisableCopyAllowPrinting();
		Optional<PdfConfiguration> pdfConfiguration = Optional.of(config);
		
		//PDF with disable copy and allow printing configuration.
		pdfConverter.createPdf(ftlFilePath, ftlModel, pdfFile, pdfConfiguration );
	}

	
	/**
	 * Create a pdf config with disable copy and allow printing PdfConfiguration object.
	 * @return pdfConfiguration object.
	 */
	private static PdfConfiguration createPdfConfigurationWithDisableCopyAllowPrinting() {
		PdfConfiguration config = new PdfConfiguration();
		config.setAllowCopying(false);
		config.setAllowPrinting(true);
		return config;
	}

	/**
	 * Fill some arbitary data that is required for the variables 
	 * used in freemarker template file.
	 * 
	 * @return Hashmap of key value objects (key should be matched with ftl variables
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static Map<String, Object> populateDataModel() throws FileNotFoundException, IOException {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("invoiceNumber", "3131");
		data.put("invoiceDate", new Date());
		data.put("recipientName", "John Doe");
		data.put("recipientCompany", "Example Inc");
		data.put("recipientEmail", "john@example.org");
		data.put("paymentMethod", "Cash");
		data.put("paymentAmount", "380");
		
		File img = new File("src/main/resources/templates/isha-foundation-logo.png");
		byte[] imgBytes = IOUtils.toByteArray(new FileInputStream(img));
		byte[] imgBytesAsBase64 = Base64.encodeBase64(imgBytes);
		String imgDataAsBase64 = new String(imgBytesAsBase64);
		String imgAsBase64 = "data:image/png;base64," + imgDataAsBase64;
		data.put("imgAsBase64", imgAsBase64);
		//List parsing 
		List<InvoiceDetail> invoices = Arrays.asList(
			       new InvoiceDetail( "Book: Source of Adiyogi", 1, 150.50 ), 
			       new InvoiceDetail( "DVD: Yoga and meditation", 2, 500.00 ) );
		
		data.put("invoices", invoices);
		return data;
	}
}

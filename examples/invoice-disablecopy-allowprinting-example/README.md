# Invoice Disable Copy Allow Printing Example

This example generate a sample Invoice PDF (with disable copying PDF contents but allow PDF printing) by making use of isha `reporting-component` library.

### Steps to run:

##### Step 1:
Import this project in eclipse as maven project.

##### Step 2:
The template file is located at `src/main/resources/templates/invoice.ftl`. 
which is freemarker template. 
 - Layouts can be changed by directly editing this template file.
 - Any change in freemarker variables needs to be changed in `InvoiceDisableCopyAllowPrintingExample.populateDataModel()`.  
  
##### Step 3:
In your caller code, (eg. InvoiceDisableCopyExample.java) modify necessary pdf configuration values. In this case PDF disable copy.
 - Create a PDFConfiguration object and set `allowCopying` value to `false`. By default this value is set to `true`. 
 - Create a PDFConfiguration object and set `allowPrinting` value to `true`. By default this value is set to `true`.
 - Pass the pdfconfiguration object to the calling code e.g `pdfConverter.createPdf(ftlFilePath, ftlModel, pdfFile, pdfConfiguration );`.  
##### Step 4: 

Run the class as Java Application `src/main/java/org/ishafoundation/invoice/InvoiceDisableCopyAllowPrintingExample.java` from your editor.





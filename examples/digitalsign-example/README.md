# Digital Sign With Password Example

This example generate a sample Invoice PDF by making use of isha `reporting-component` library. The generated PDF is digitally signed and protected with password.

### Steps to run:

##### Step 1:
Import this project in eclipse as maven project.

##### Step 2:
The template file is located at `src/main/resources/templates/invoice.ftl`. 
which is freemarker template. 
 - Layouts can be changed by directly editing this template file.
 - Any change in freemarker variables needs to be changed in `DigitalSignExample.populateDataModel()`.   

##### Step 3:
In order to digitally sign, first need to create a keystore using following command
```keytool -genkey -alias demo -keyalg RSA -keystore ks -keysize 2048```

The generated keystore file (in this case ***ks***) should be stored under ```src/main/resources/``` folder. 

Configure the filename, path, password etc by changing the constants at ```DigitalSignExample.java```


##### Step 4: 

Run the class `src/main/java/org/ishafoundation/example/DigitalSignExample.java` from your editor.





# Invoice Example

This example generate a sample Invoice PDF by making use of isha `reporting-component` library

### Steps to run:

##### Step 1:
Import this project in eclipse as maven project.

##### Step 2:
The template file is located at `src/main/resources/templates/invoice.ftl`. 
which is freemarker template. 
 - Layouts can be changed by directly editing this template file.
 - Any change in freemarker variables needs to be changed in `InvoiceExample.populateDataModel()`.   

##### Step 3: 

Run the class `src/main/java/org/ishafoundation/invoice/InvoiceExample.java` from your editor.
# Invoice Example

This example generate a sample Invoice PDF (with password protection) by making use of isha `reporting-component` library.



### Steps to run:

##### Step 1:
Import this project in eclipse as maven project.

##### Step 2:
The template file is located at `src/main/resources/templates/invoice.ftl`. 
which is freemarker template. 
 - Layouts can be changed by directly editing this template file.
 - Any change in freemarker variables needs to be changed in `InvoiceExample.populateDataModel()`.  
  
##### Step 3:
In your caller code, (eg. InvoicePasswordExample.java) set necessary pdf configuration values. In this case PDF password.
 - Create a PDFConfiguration object and set `passwordProtected` value to `true`. 
 - Set the value of password to `passwordText` to desired value.
 - Pass the pdfconfiguration object to the calling code e.g `pdfConverter.createPdf(ftlFilePath, ftlModel, pdfFile, pdfConfiguration );`.

### NOTE: 
 Setting `passwordProtected` to true will override the properties of **`allowPrinting`/ `allowCopying`.** In otherwords, `allowPrinting`/ `allowCopying` will work only if there is no password protection is there. 
  
##### Step 4: 

Run the class `src/main/java/org/ishafoundation/invoice/InvoicePasswordExample.java` from your editor.






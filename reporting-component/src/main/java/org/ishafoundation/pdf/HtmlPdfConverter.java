package org.ishafoundation.pdf;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public interface HtmlPdfConverter {
	
	/**
	 * Creates a pdf from source freemarker template file. 
	 * @param sourceFtlFilePath source file path of freemarker template
	 * @param ftlModel freemarker data model, for the variables used in template
	 * @param destPdfFile file destination of generated pdf file 
	 * @param pdfConfiguration Pdf configuration for the generted pdf 
	 * @throws IOException when some file error occurs
	 */
	public void createPdf(String sourceFtlFilePath, Map<String, Object> ftlModel, String destPdfFile, Optional<PdfConfiguration> pdfConfiguration) throws IOException;

}
package org.ishafoundation.pdf;

public class PdfConfiguration {
	
	private boolean allowPrinting = true;
	private boolean allowCopying = true;
	private boolean digitallySigned;
	private String digitalSignKeyPassword;
	private String keyStorePath;
	
	private boolean passwordProtected;
	private String passwordText = "Password"; //Default password
	
	public boolean isAllowPrinting() {
		return allowPrinting;
	}
	public void setAllowPrinting(boolean allowPrinting) {
		this.allowPrinting = allowPrinting;
	}
	public boolean isAllowCopying() {
		return allowCopying;
	}
	public void setAllowCopying(boolean allowCopying) {
		this.allowCopying = allowCopying;
	}
	public boolean isDigitallySigned() {
		return digitallySigned;
	}
	public void setDigitallySigned(boolean digitallySigned) {
		this.digitallySigned = digitallySigned;
	}
	public String getDigitalSignKeyPassword() {
		return digitalSignKeyPassword;
	}
	public void setDigitalSignKeyPassword(String digitalSignKeyPassword) {
		this.digitalSignKeyPassword = digitalSignKeyPassword;
	}
	public String getKeyStorePath() {
		return keyStorePath;
	}
	public void setKeyStorePath(String keyStorePath) {
		this.keyStorePath = keyStorePath;
	}
	public boolean isPasswordProtected() {
		return passwordProtected;
	}
	public void setPasswordProtected(boolean passwordProtected) {
		this.passwordProtected = passwordProtected;
	}
	public String getPasswordText() {
		return passwordText;
	}
	public void setPasswordText(String passwordText) {
		this.passwordText = passwordText;
	}
}

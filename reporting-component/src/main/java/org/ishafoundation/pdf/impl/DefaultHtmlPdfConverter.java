package org.ishafoundation.pdf.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ishafoundation.pdf.HtmlPdfConverter;
import org.ishafoundation.pdf.PdfConfiguration;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.EncryptionConstants;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.ReaderProperties;
import com.itextpdf.kernel.pdf.WriterProperties;
import com.itextpdf.signatures.BouncyCastleDigest;
import com.itextpdf.signatures.DigestAlgorithms;
import com.itextpdf.signatures.IExternalDigest;
import com.itextpdf.signatures.IExternalSignature;
import com.itextpdf.signatures.PdfSignatureAppearance;
import com.itextpdf.signatures.PdfSigner;
import com.itextpdf.signatures.PrivateKeySignature;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Default implementation of HtmltoPdfConverter. 
 * @author nagavijay.s
 *
 */
public class DefaultHtmlPdfConverter implements HtmlPdfConverter {

	@Override
	public void createPdf(String sourceFtlFilePath, Map<String, Object> ftlModel, String destPdfFile,
			Optional<PdfConfiguration> pdfConfiguration) throws IOException {

		Configuration cfg = new Configuration();

		try {
			// Load template from source folder
			Template template = cfg.getTemplate(sourceFtlFilePath);
			// File output
			String htmlFileName = System.getProperty("java.io.tmpdir") + File.separator + "ftl_generated.html";
			File htmlFile = new File(htmlFileName);
			Writer writer = new FileWriter(htmlFile);
			template.process(ftlModel, writer);
			writer.flush();
			writer.close();

			HtmlConverter.convertToPdf(htmlFile, new File(destPdfFile));

			PdfConfiguration pdfConfig;
			if (Optional.ofNullable(pdfConfiguration).isPresent()) {
				pdfConfig = pdfConfiguration.get();
			} else {
				pdfConfig = new PdfConfiguration();
			}

			ConverterProperties converterProperties = new ConverterProperties();

			convertPDFWithConfiguration(destPdfFile, htmlFileName, pdfConfig, converterProperties);
			if (pdfConfig.isDigitallySigned()) {
				convertPDFWithDigitalSignature(destPdfFile, pdfConfig);
			} 

		} catch (IOException | TemplateException | GeneralSecurityException e) {
			System.out.println("Error while generating pdf." + e.getMessage());
			e.printStackTrace();
		}

	}


	private void convertPDFWithConfiguration(String destPdfFile, String htmlFileName, PdfConfiguration pdfConfig,
			ConverterProperties converterProperties) throws FileNotFoundException, IOException {
		WriterProperties writerProperties = new WriterProperties();
		
		int permissions = 0;
		permissions |= pdfConfig.isAllowPrinting() ? EncryptionConstants.ALLOW_PRINTING : permissions;
		permissions |= pdfConfig.isAllowCopying() ? EncryptionConstants.ALLOW_COPY : permissions;
		byte[] passwordBytes = null;
		passwordBytes = pdfConfig.isPasswordProtected() ? pdfConfig.getPasswordText().getBytes() : passwordBytes;
		writerProperties.setStandardEncryption(passwordBytes, passwordBytes, permissions, EncryptionConstants.ENCRYPTION_AES_128);
		PdfWriter pdfWriter = new PdfWriter(destPdfFile, writerProperties);
		HtmlConverter.convertToPdf(new FileInputStream(htmlFileName), pdfWriter, converterProperties);
		pdfWriter.close();
	}

	private void convertPDFWithDigitalSignature(String destPdfFile, PdfConfiguration pdfConfig) throws FileNotFoundException, IOException, GeneralSecurityException {
		BouncyCastleProvider provider = new BouncyCastleProvider();
		Security.addProvider(provider);
		KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		ks.load(new FileInputStream(pdfConfig.getKeyStorePath()), pdfConfig.getDigitalSignKeyPassword().toCharArray());
		String alias = ks.aliases().nextElement();
		PrivateKey pk = (PrivateKey) ks.getKey(alias, pdfConfig.getDigitalSignKeyPassword().toCharArray());
		Certificate[] chain = ks.getCertificateChain(alias);
		String newTempFile = FilenameUtils.removeExtension(destPdfFile) + "_signed.pdf";
		sign(destPdfFile, newTempFile, chain, pk, pdfConfig.getPasswordText(), DigestAlgorithms.SHA256, provider.getName(), PdfSigner.CryptoStandard.CMS);
		FileUtils.deleteQuietly(new File(destPdfFile));
		new File(newTempFile).renameTo(new File(destPdfFile));

	}

	public void sign(String src, String dest, Certificate[] chain, PrivateKey pk, String pdfPassword, String digestAlgorithm,
			String provider,  PdfSigner.CryptoStandard subfilter)
			throws GeneralSecurityException, IOException {
		ReaderProperties readerproperties= new ReaderProperties();
		readerproperties.setPassword(pdfPassword.getBytes());
		// Creating the reader and the signer
		PdfReader reader = new PdfReader(src, readerproperties);
		PdfSigner signer = new PdfSigner(reader, new FileOutputStream(dest), false);
		// Creating the appearance
		PdfSignatureAppearance appearance = signer.getSignatureAppearance().setReuseAppearance(false);
		appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.DESCRIPTION);
		Rectangle rect = new Rectangle(0, 0, 0, 0); //Hide the digital sign label
		appearance.setPageRect(rect).setPageNumber(1);
		signer.setFieldName("sig");
		// Creating the signature
		IExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
		IExternalDigest digest = new BouncyCastleDigest();
		signer.signDetached(digest, pks, chain, null, null, null, 0, subfilter);
	}
}
